package se.sensera.banking.Imp;

import se.sensera.banking.*;
import se.sensera.banking.exceptions.Activity;
import se.sensera.banking.exceptions.UseException;
import se.sensera.banking.exceptions.UseExceptionType;
import se.sensera.banking.utils.ListUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class AccountServiceImpl implements AccountService {
    AccountsRepository accountsRepository;
    UsersRepository usersRepository;

    public AccountServiceImpl(AccountsRepository accountsRepository, UsersRepository usersRepository) {
        this.accountsRepository = accountsRepository;
        this.usersRepository = usersRepository;
    }

    @Override
    public Account createAccount(String userId, String accountName) throws UseException {
        User user = usersRepository.getEntityById(userId).
                orElseThrow(() -> new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND));
        if (accountsRepository.all().anyMatch(account -> account.getOwner().equals(user))) {
            throw new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.ACCOUNT_NAME_NOT_UNIQUE);
        }
        AccountImpl account = new AccountImpl(UUID.randomUUID().toString(), user, accountName, true, new ArrayList<>());
        accountsRepository.save(account);
        return account;
    }

    @Override
    public Account changeAccount(String userId, String accountId, Consumer<ChangeAccount> changeAccountConsumer) throws UseException {
        User owner = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND));
        Account account = accountsRepository.getEntityById(accountId)
                .orElseThrow(() -> new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.ACCOUNT_NOT_FOUND));
        if (!account.getOwner().equals(owner)) {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_OWNER);
        }
        if (!account.isActive()) {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_ACTIVE);
        }

        account.setActive(false);
        AtomicBoolean saveAccount = new AtomicBoolean(false);
        changeAccountConsumer.accept(new ChangeAccount() {
            @Override
            public void setName(String name) throws UseException {
                if (accountsRepository.all().anyMatch(account1 -> account1.getName().equals(name))) {
                    throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.ACCOUNT_NAME_NOT_UNIQUE);
                }
                if (!account.getName().equals(name)) {
                    account.setName(name);
                    saveAccount.set(true);
                }
            }
        });
        if (saveAccount.get()) {
            accountsRepository.save(account);
        }
        return account;
    }

    @Override
    public Account addUserToAccount(String userId, String accountId, String userIdToBeAssigned) throws UseException {
        Optional<User> fromUser = usersRepository.getEntityById(userId);
        User user = fromUser.get();
        Optional<User> otherUser = usersRepository.getEntityById(userIdToBeAssigned);
        User userToBe = otherUser.get();

        Account account = accountsRepository.getEntityById(accountId)
                .orElseThrow(() -> new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_FOUND));
        if (!account.isActive()) {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.ACCOUNT_NOT_ACTIVE);
        }
        if (account.getOwner().equals(userToBe)) {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.CANNOT_ADD_OWNER_AS_USER);
        }
        if (!account.getOwner().equals(user)) {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_OWNER);
        }
        if (account.getUsers().anyMatch(a -> a.equals(userToBe))) {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.USER_ALREADY_ASSIGNED_TO_THIS_ACCOUNT);
        }
        accountsRepository.save(account);
        account.addUser(userToBe);
        return account;
    }

    @Override
    public Account removeUserFromAccount(String userId, String accountId, String userIdToBeAssigned) throws UseException {
        Optional<User> userToBe = usersRepository.getEntityById(userIdToBeAssigned);
        User otherUser = userToBe.get();
        Optional<User> user = usersRepository.getEntityById(userId);
        User user2 = user.get();
        Account account = accountsRepository.getEntityById(accountId).get();

        if (!account.getOwner().equals(user2)) {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_OWNER);
        }
        if (account.getUsers().noneMatch(u -> u.getId().equals(userIdToBeAssigned))) {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.USER_NOT_ASSIGNED_TO_THIS_ACCOUNT);
        }

        accountsRepository.save(account);
        account.removeUser(otherUser);
        return account;
    }

    @Override
    public Account inactivateAccount(String userId, String accountId) throws UseException {
        verifyActiveUser(userId, Activity.INACTIVATE_ACCOUNT);
        Account account = getActiveOwnedAccount(Activity.INACTIVATE_ACCOUNT, userId, accountId);
        account.setActive(false);
        return accountsRepository.save(account);
    }

    private User verifyActiveUser(String userId, Activity updateAccount) throws UseException {
        User user = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(updateAccount, UseExceptionType.USER_NOT_FOUND));
        if (!user.isActive())
            throw new UseException(updateAccount, UseExceptionType.NOT_ACTIVE);
        return user;
    }

    private Account getActiveOwnedAccount(Activity activity, String userId, String accountId) throws UseException {
        Account account = accountsRepository.getEntityById(accountId)
                .orElseThrow(() -> new UseException(activity, UseExceptionType.NOT_FOUND));
        if (!account.getOwner().getId().equals(userId))
            throw new UseException(activity, UseExceptionType.NOT_OWNER);
        if (!account.isActive())
            throw new UseException(activity, UseExceptionType.NOT_ACTIVE);
        return account;
    }

    @Override
    public Stream<Account> findAccounts(String searchValue, String userId, Integer pageNumber, Integer pageSize, SortOrder sortOrder) throws UseException {
        Stream<Account> all = accountsRepository.all()
                .filter(account -> account.getName().contains(searchValue));
        if (all == null) {
            throw new UseException(Activity.FIND_ACCOUNT, UseExceptionType.ACCOUNT_NOT_FOUND);
        }
        if (sortOrder.equals(SortOrder.AccountName)) {
            all = all.sorted(Comparator.comparing(Account::getName));
        }
        if (userId != null) {
            all = all.filter(account -> account.getOwner().getId().equals(userId) || account.getUsers().anyMatch(user -> user.getId().equals(userId)));
        }
        return ListUtils.applyPage(all, pageNumber, pageSize);
    }
}