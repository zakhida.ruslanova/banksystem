package se.sensera.banking.Imp;

import se.sensera.banking.User;
import se.sensera.banking.UserService;
import se.sensera.banking.UsersRepository;
import se.sensera.banking.exceptions.Activity;
import se.sensera.banking.exceptions.UseException;
import se.sensera.banking.exceptions.UseExceptionType;
import se.sensera.banking.utils.ListUtils;

import java.util.Comparator;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class UserServiceImpl implements UserService {

    UsersRepository usersRepository;

    public UserServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public User createUser(String name, String personalIdentificationNumber) throws UseException {
        if (usersRepository.all().anyMatch(user -> user.getPersonalIdentificationNumber().equals(personalIdentificationNumber))) //hämtar alla objekt och matchar om det finns i databasen.
            throw new UseException(Activity.CREATE_USER, UseExceptionType.USER_PERSONAL_ID_NOT_UNIQUE);
        UserImpl user = new UserImpl(UUID.randomUUID().toString(), name, personalIdentificationNumber, true);
        return usersRepository.save(user);
    }

    @Override
    public User changeUser(String userId, Consumer<ChangeUser> changeUser) throws UseException {
        User user = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.UPDATE_USER, UseExceptionType.NOT_FOUND));
        AtomicBoolean saveUser = new AtomicBoolean(false);
        changeUser.accept(new ChangeUser() {
            @Override
            public void setName(String name) {
                user.setName(name);
                saveUser.set(true);
            }

            @Override
            public void setPersonalIdentificationNumber(String personalIdentificationNumber) throws UseException {
                if (usersRepository.all().anyMatch(user -> user.getPersonalIdentificationNumber().equals(personalIdentificationNumber)))
                    throw new UseException(Activity.UPDATE_USER, UseExceptionType.USER_PERSONAL_ID_NOT_UNIQUE);
                user.setPersonalIdentificationNumber(personalIdentificationNumber);
                saveUser.set(true);
            }
        });
        if (saveUser.get()) {
            usersRepository.save(user);
        }
        return user;
    }

    @Override
    public User inactivateUser(String userId) throws UseException {
        User user = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.UPDATE_USER, UseExceptionType.NOT_FOUND));
        user.setActive(false);
        return usersRepository.save(user);
    }

    @Override
    public Optional<User> getUser(String userId) {
        Optional user = usersRepository.getEntityById(userId);
        return user;
    }

    @Override
    public Stream<User> find(String searchString, Integer pageNumber, Integer pageSize, SortOrder sortOrder) {
        boolean isGetAll = true;
        boolean isGetAllActive = false;
        if (searchString.equals("") && pageNumber == null && pageSize == null && sortOrder.equals(UserService.SortOrder.None)) {
            if (searchString.equals("") && pageNumber == null && pageSize == null && sortOrder.equals(UserService.SortOrder.None) && (!isGetAll && !isGetAllActive)) {
                return usersRepository.all();
            } else {
                if (pageNumber == null && pageSize == null && sortOrder.equals(SortOrder.None)) ;
                {
                    return usersRepository.all().filter(User::isActive);
                }
            }
        }
        if (!searchString.equals("") && pageNumber == null && pageSize == null && sortOrder.equals(UserService.SortOrder.None)) {
            return usersRepository.all().filter(user -> user.getName().toLowerCase().contains(searchString));
        }
        if (!searchString.equals("") && pageNumber != null && pageSize == null && sortOrder.equals(UserService.SortOrder.None)) {
            Stream<User> userStream = usersRepository.all().filter(user -> user.getName().toLowerCase().contains(searchString));
            return ListUtils.applyPage(userStream, pageNumber, pageSize);
        }
        if (!searchString.equals("") && pageNumber != null && pageSize != null && sortOrder.equals(UserService.SortOrder.None)) {
            Stream<User> userStream = usersRepository.all().filter(user -> user.getName().toLowerCase().contains(searchString));
            return ListUtils.applyPage(userStream, pageNumber, pageSize);
        }
        if (searchString.equals("") && pageNumber == null && pageSize == null && sortOrder.equals(SortOrder.Name)) {
            return usersRepository.all().sorted(Comparator.comparing(User::getName));
        }
        if (searchString.equals("") && pageNumber == null && pageSize == null && sortOrder.equals(SortOrder.PersonalId)) {
            return usersRepository.all().sorted(Comparator.comparing(User::getPersonalIdentificationNumber));
        }
        if (!searchString.equals("") && pageNumber == null && pageSize == null && sortOrder.equals(SortOrder.None)) {
            return usersRepository.all().filter(user -> user.getName().toUpperCase().contains(searchString));
        }
        if (searchString.equals("") && pageNumber != null && pageSize != null && sortOrder.equals(UserService.SortOrder.None)) {
            Stream<User> userStream = usersRepository.all();
            return ListUtils.applyPage(userStream, pageNumber, pageSize);
        }
        return null;
    }
}