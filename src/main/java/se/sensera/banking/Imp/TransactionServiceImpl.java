package se.sensera.banking.Imp;

import lombok.AllArgsConstructor;
import lombok.Data;
import se.sensera.banking.*;
import se.sensera.banking.exceptions.Activity;
import se.sensera.banking.exceptions.UseException;
import se.sensera.banking.exceptions.UseExceptionType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
public class TransactionServiceImpl implements se.sensera.banking.TransactionService {
    UsersRepository usersRepository;
    AccountsRepository accountsRepository;
    TransactionsRepository transactionsRepository;
    static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    List<Consumer<Transaction>> consumerTransactionList = new ArrayList<>();

    public TransactionServiceImpl(UsersRepository usersRepository, AccountsRepository accountsRepository, TransactionsRepository transactionsRepository) {
        this.usersRepository = usersRepository;
        this.accountsRepository = accountsRepository;
        this.transactionsRepository = transactionsRepository;
    }

    @Override
    public Transaction createTransaction(String created, String userId, String accountId, double amount) throws UseException {
        User user = usersRepository.getEntityById(userId).get();
        Account account = accountsRepository.getEntityById(accountId).get();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = null;
        try {
            date = formatter.parse(created);
        } catch (ParseException e) {
            throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_FOUND);
        }
        if (!account.getOwner().equals(user)) {
            if (account.getUsers().noneMatch(a -> a.getId().equals(userId))) {
                throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_ALLOWED);
            }
        }

        Transaction transaction = new TransactionImpl(UUID.randomUUID().toString(), date, user, account, amount);
        if (transaction.getAmount() < -101) {
            throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_FUNDED);
        }

        transactionsRepository.save(transaction);
        consumerTransactionList.forEach(transactionConsumer -> transactionConsumer.accept(transaction));
        return transaction;
    }

    @Override
    public double sum(String created, String userId, String accountId) throws UseException {
        Date date;
        try {
            date = format.parse(created);
        }
        catch (ParseException e) {
            throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_FOUND);
        }
        Date controlledDate = date;
        usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.CREATE_USER, UseExceptionType.NOT_FOUND));
        Account account = accountsRepository.getEntityById(accountId)
                .orElseThrow(() -> new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.NOT_FOUND));

        if (!account.getOwner().getId().equals(userId) &&
                account.getUsers().noneMatch(user1 -> user1.getId().equals(userId))) {
            throw new UseException(Activity.SUM_TRANSACTION, UseExceptionType.NOT_ALLOWED);
        }
        return transactionsRepository.all()
                .filter(transaction -> transaction.getAccount().getId().equals(accountId))
                .filter(transaction -> transaction.getCreated().compareTo(controlledDate) <= 0)
                .mapToDouble(Transaction::getAmount).sum();
    }

    @Override
    public void addMonitor(Consumer<Transaction> monitor) {
        consumerTransactionList.add(monitor);
    }
}