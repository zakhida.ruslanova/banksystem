package se.sensera.banking.Imp;

import lombok.AllArgsConstructor;
import lombok.Data;
import se.sensera.banking.Account;
import se.sensera.banking.User;

import java.util.List;
import java.util.stream.Stream;

@Data
@AllArgsConstructor
public class AccountImpl implements Account {
    String id;
    User owner;
    String name;
    boolean active;
    List<User> usersList;

    @Override
    public Stream<User> getUsers() {
        return usersList.stream();
    }

    @Override
    public void addUser(User user) { usersList.add(user); }

    @Override
    public void removeUser(User user) {
        usersList.remove(user);
    }
}